import WeatherForecast from './WeatherForecast';

import './App.css';

function App() {
  return (
    <>
      <WeatherForecast />
    </>
  );
}

export default App;
