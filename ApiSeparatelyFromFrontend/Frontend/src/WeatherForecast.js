import React, { useState } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import CloudIcon from '@material-ui/icons/Cloud';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import API from "./utils/API";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';


const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.primary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    tableContainer: {

        margin: theme.spacing(0),
        // marginLeft: theme.spacing(20)
    }
}));




export default function WeatherForecast() {
    const classes = useStyles();

    const [forecasts, setForecasts] = useState([]);

    const onSubmit = async (e) => {
        e.preventDefault();

        let userData = await API.get('/WeatherForecast');
        setForecasts(userData.data);
        console.log(userData.data);
    };

    return (
        <>
            <Container component="main" maxWidth="xs">
                <CssBaseline />
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                        <CloudIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        ПОГОДА
                </Typography>

                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="secondary"
                        className={classes.submit}
                        onClick={onSubmit}
                    >Обновить</Button>

                </div>

            </Container>
            <TableContainer component={Paper} className={classes.tableContainer}>
                <Table className={classes.table} size="small" aria-label="a dense table">
                    <TableHead>
                        <TableRow>
                            <TableCell align="right">Date</TableCell>
                            <TableCell align="right">Temp. (C)</TableCell>
                            <TableCell align="right">Temp. (F)</TableCell>
                            <TableCell align="right">Summary</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {forecasts.map((forecast) => (
                            <TableRow key={forecast.date}>
                                <TableCell align="right">{forecast.date}</TableCell>
                                <TableCell align="right">{forecast.temperatureC}</TableCell>
                                <TableCell align="right">{forecast.temperatureF}</TableCell>
                                <TableCell align="right">{forecast.summary}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </>
    );
}